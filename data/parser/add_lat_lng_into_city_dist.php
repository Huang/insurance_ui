<?php
$list = file('/tmp/city_dist.json');
foreach($list as $l) {
  if (strstr($l, 'dist') === FALSE) {
    echo $l;
    continue;
  }

  $a = split('\[', $l)[0];
  $b = split('\]', split('\[', $l)[1])[0];
  $c = split('\]', split('\[', $l)[1])[1];

  $g = get_city($a);
  $d = split(',', $b);
  $f = '';
  foreach ($d as $e) {
    $h = get_dist($e);
    if (is_file("kml/area_center/result-$g$h.json")) {
      $i = fopen("kml/area_center/result-$g$h.json", 'r');
      $j = json_decode(fread($i, filesize("kml/area_center/result-$g$h.json")));
      $k = array_pop($j->results);
      $lat = $k->geometry->location->lat;
      $lng = $k->geometry->location->lng;
    }
    $f .= "{'name':$e, 'latlng': {'lat': $lat, 'lng': $lng}}, ";
  }
  $f = substr($f, 0, -2);
  echo $a.'['.$f.']'.$c;
}

function get_city($a) {
  preg_match('#: \'(.+)\', #', $a, $m);
  return $m[1];
}

function get_dist($e) {
  preg_match('#\'(.+)\'#', $e, $m);
  return $m[1];
}

<?php
# {
#   "districts": [
#     {
#       "district": "臺北市大同區",
#       "causes": [
#         {
#           "cause": "惡性腫瘤",
#           "sex": [
#             {
#               "male": [
#                 {
#                   "age_code": 22,
#                   "count": 3 
#                 },
#                 {
#                   ...
#                 }
#               ],
#               "female": [
#                 {
#                   "age_code": 22,
#                   "count": 2 
#                 },
#                 {
#                   ...
#                 }
#               ]
#             }
#           ]
#         },
#         {
#           ...
#         }
#       ]
#     },
#     {
#       ...
#     }
#   ]
# }
if ($argv[2] < 97)
  $cause_file = "../cause_lookup_before97.json";
else
  $cause_file = "../cause_lookup.json";

$fs = fopen($cause_file, 'r');
$cause_lookup = json_decode(fread($fs, filesize($cause_file)), true);
fclose($fs);

$fs = fopen('../dist_lookup.json', 'r');
$dist_lookup = json_decode(fread($fs, filesize('../dist_lookup.json')), true);
fclose($fs);

$fs = fopen('../age_lookup.json', 'r');
$age_lookup = json_decode(fread($fs, filesize('../age_lookup.json')), true);
fclose($fs);

$lines = file($argv[1]);
$all = array();
foreach ($lines as $line) {
  if (!is_numeric($line[0]))
    continue;
  $arr = explode(',', trim($line));
  #1=dist,2=cause,3=sex,4=age_code,5=n
  $dist = trans_dist($arr[1]);
  $cause = trans_cause($arr[2]);
  $sex = trans_sex($arr[3]);
  $age = trans_age_code($arr[4]);

  if (! find_key_value($all, 'dist_name', $dist, $i)) {
    $all[] = array('dist_name'=>$dist, 'data'=>array());
    find_key_value($all, 'dist_name', $dist, $i);
  }
  $cur_dist = &$all[$i]['data'];

  if (! find_key_value($cur_dist, 'cause_name', $cause, $i)) {
    $cur_dist[] = array('cause_name'=>$cause, 'data'=>array());
    find_key_value($cur_dist, 'cause_name', $cause, $i);
  }
  $cur_cause = &$cur_dist[$i]['data'];

  if (! find_key_value($cur_cause, 'sex_name', $sex, $i)) {
    $cur_cause[] = array('sex_name'=>$sex, 'data'=>array());
    find_key_value($cur_cause, 'sex_name', $sex, $i);
  }
  $cur_sex = &$cur_cause[$i]['data'];

  $cur_sex[] = array("age_code"=>$age, "count"=>$arr[5]);
  unset($i);
  unset($cur_sex);
  unset($cur_cause);
  unset($cur_dist);
}
echo json_encode($all);


function find_key_value($arr, $key, $val, &$ret = null) {
  for ($i = 0 ; $i < count($arr) ; $i++) {
    if ($arr[$i][$key] == $val) {
      $ret = $i;
      return true;
    }
  }
  $ret = null;
  return false;
}

function trans_dist($str) {
  global $dist_lookup;
  foreach ($dist_lookup as $k=>$v) {
    for ($i = 1 ; $i <= 4 ; $i++)
      if (@$v['id'.$i] == $str)
        return $v['name'];
  }
  return null;
}

function trans_cause($str) {
  global $cause_lookup;
  foreach ($cause_lookup as $k=>$v) {
    if ($v['id'] == $str)
      return $v['name'];
  }
  return null;
}

function trans_age_code($str) {
  global $age_lookup;
  foreach ($age_lookup as $k=>$v) {
    if ($v['agecode'] == $str)
      return $v['name'];
  }
  return null;
}

function trans_sex($str) {
  return ($str=='1'?'男':'女');
}

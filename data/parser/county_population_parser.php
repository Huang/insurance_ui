<?php
$cap_arr = array(
  '', 'total', '', 'y1_4', '', '', '', '', 'y5_9', 'y10_14',
  'y15_19', 'y20_24', 'y25_29', 'y30_34', 'y35_39', 'y40_44',
  'y45_49', 'y50_54', 'y55_59', 'y60_64', 'y65_69', 'y70_74',
  'y75_79', 'y80_84', 'y85_89', 'y90_94', 'y95_99', 'y100_'
);

$f = file('t1');
$json = array();
foreach($f as $k=>$v) {
  if (preg_match('/中 *華 *民 *國 *([0-9]+) *年 *底/', $v, $match)) {
    $year = (int)$match[1];
    continue;
  }
  if (strpos($v, '  男 ') !== false) {
    $county = trim(preg_replace('/(　| )+/','', $f[$k-1]));
    if ($county == '總計')
      continue;
    $in_county = array();
    $in_county['year'] = $year;
    $in_county['county'] = $county;
    $in_county['population'] = array(
      array('sex'=>'男', 'data'=>array()), 
      array('sex'=>'女', 'data'=>array())
    );
    $in_county['population'][0]['data'] = split_line($f[$k]);
    $in_county['population'][1]['data'] = split_line($f[$k+1]);
    $json[] = $in_county;
  }
}
print json_encode($json);
exit;

function split_line($str) {
  global $cap_arr;
  $t = split(' ', trim($str));
  $arr = array();
  foreach($t as $k=>$v) {
    if ($cap_arr[$k] != '') {
      $arr[$cap_arr[$k]] = is_numeric($v) ? (int)$v : $v;
    }
  }
  return $arr;
}

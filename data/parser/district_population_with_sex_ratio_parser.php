<?php
$fp = fopen($argv[1], 'r');
$dist_json = json_decode(fread($fp, filesize($argv[1])), true);
fclose($fp);
$fp = fopen($argv[2], 'r');
$county_json = json_decode(fread($fp, filesize($argv[2])), true);
fclose($fp);

$arr = array();
foreach($county_json as $v) {
  if ($v['population'][0]['sex'] == '男') {
    $m = $v['population'][0]['data']['total'];
    $f = $v['population'][1]['data']['total'];
  } else {
    $m = $v['population'][1]['data']['total'];
    $f = $v['population'][0]['data']['total'];
  }
  $sex_ratio = @sprintf("%.4f", $m/$f);
  $arr[] = array('county'=>$v['county'], 'year'=>$v['year'], 'sex_ratio'=>$sex_ratio);
}

#echo json_encode($arr);

foreach($dist_json as $k=>$v) {
  $year = date("Y", $v['timestamp']) - 1911;
  $county = substr($v['name'], 0, 9);
  $dist_json[$k]['county_sex_ratio'] = get_ratio_from_year_and_county($arr, $year, $county);
}

echo json_encode($dist_json);

function get_ratio_from_year_and_county($arr, $year, $county) {
  $old_county = array('新北市'=>'臺北縣', '臺中市'=>'臺中縣', '高雄市'=>'高雄縣', '臺南市'=>'臺南縣');
  foreach($arr as $v) {
    if (($v['county'] == $county || $v['county'] == @$old_county[$county]) && $v['year'] == $year) {
      return $v['sex_ratio'];
    }
  }
}

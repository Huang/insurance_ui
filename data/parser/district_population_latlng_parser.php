<?php
$f = file('t2');
$json = array();
foreach($f as $k=>$v) {
  $arr = split("\t", $v);
  if ($arr[1]) {
    $county_name = $arr[1];
    continue;
  }
  if ($arr[2]) {
    $district = array();
    $district['name'] = $county_name . $arr[2];
    $fs = fopen('/home/xbddc/nfs/work/insurance/kml/area_center/result-' . ($district['name']) . '.json', 'r');
    $latlng_data = json_decode(fread($fs, 102400), true);
    $latlng_data = $latlng_data['results'][0]['geometry']['location'];
    fclose($fs);

    $district['latlng'] = $latlng_data;
    for($i = 3 ; $i < count($arr) ; $i++) {
      $district['population'] = (int)trim(str_replace(',', '', $arr[$i]));
      $district['timestamp'] = mktime(23, 59, 59, 12, 31, $i+77+1911);
      $json[] = $district;
    }
  }
}
print json_encode($json);

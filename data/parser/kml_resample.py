#!/usr/bin/env python
# -*- encoding: utf8 -*-

import sys
import optparse
import time
import re


__author__ = 'Chandler Huang <previa@gmail.com>'


def main(args):
    '''\
    %prog [options]
    '''
    output_buffer = []
    filter_counter = 0
    filter_ratio = int(args[0])
    with open (args[1], "r") as myfile:
        data=myfile.read()
        token_list = data.split(" ")
        for token in token_list:
            if re.match("^\d+?\.\d+?,\d+?\.\d+?,\d+?\.\d+?$", token) is not None:
                filter_counter += 1
                if filter_counter % filter_ratio == 0:
                    output_buffer.append(token)

            else:
                filter_counter = 0
                output_buffer.append(token)

    text_file = open(args[2], "w")
    text_file.write(" ".join(output_buffer))
    text_file.close()
    return 0


if __name__ == '__main__':
    parser = optparse.OptionParser(usage=main.__doc__)
    options, args = parser.parse_args()

    if len(args) != 3:
        parser.print_help()
        sys.exit(1)

    sys.exit(main(args))

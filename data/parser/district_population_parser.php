<?php
$f = file('t2');
$json = array();
foreach($f as $k=>$v) {
  $arr = split("\t", $v);
  if ($arr[1]) {
    $county_name = $arr[1];
    continue;
  }
  if ($arr[2]) {
    $district = array();
    $district['name'] = $county_name . $arr[2];
    $district['data'] = array();
    for($i = 3 ; $i < count($arr) ; $i++) {
      $district['data'][$i+77] = (int)trim(str_replace(',', '', $arr[$i]));
    }
    $json[] = $district;
  }
}
print json_encode($json);

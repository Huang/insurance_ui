#!/bin/sh

php dump_phase1.php $1 > x0
cat x0 | grep -e cause_name -e count -e dist -A1 > x1
grep -v '\[' x1 > x2
php dump_phase2.php x2 $2 > x3
php dump_phase3.php x3 $2

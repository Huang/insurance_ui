#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from flask import Flask, url_for, render_template, request, session, redirect, abort, Response

app = Flask(__name__)

@app.route("/")
def index():
    return render_template('index.html')

@app.route("/mock")
def mock():
    return render_template('mock.html')

@app.route("/center/<name>/")
def center(name):
    if os.path.isfile('./kml/area_center/result-%s.json' % name):
        buf = open('./kml/area_center/result-%s.json' % name).read()
        return Response(buf, mimetype='application/json')
    return '', 404

@app.route("/area/<name>/<ratio>/")
def area(name, ratio):
    if not os.path.isfile('./kml/area_center/result-%s.json' % name):
        return '', 404
    if not os.path.isfile('./kml/kml_%s/%s.kml' % (ratio, name)):
        return '', 404

    buf = open('./kml/kml_%s/%s.kml' % (ratio, name)).read()
    return buf.replace('330030ff', '55ffffff')

if __name__ == "__main__":
    app.secret_key = 'zooz'
    app.run(host='0.0.0.0', port=9090, threaded=True)

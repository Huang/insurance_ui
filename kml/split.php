<?php

$xml = simplexml_load_file('319.kml');
$dxml = dom_import_simplexml($xml);
$curr_placemark = "";
foreach($xml->Document->Placemark as $p) {
  if ($curr_placemark != $p->name) {
    $curr_placemark = $p->name;
    echo "switch to placemark {$curr_placemark}\n";
    if (is_file("output/{$p->name}.kml"))
      $template_xml = simplexml_load_file("output/{$p->name}.kml");
    else
      $template_xml = simplexml_load_file('template.kml');
  }
  $template_xml->Document->name = $p->name;
  if ($p->Polygon) {
    $np = $template_xml->Document->addChild('Placemark');
    $np->addChild('Polygon')->addChild('outerBoundaryIs')->addChild('LinearRing')->addChild('coordinates', $p->Polygon->outerBoundaryIs->LinearRing->coordinates);
  }
  if ($p->MultiGeometry) {
    $np = $template_xml->Document->addChild('Placemark')->addChild('MultiGeometry');
    foreach ($p->MultiGeometry->Polygon as $pp) {
      $np->addChild('Polygon')->addChild('outerBoundaryIs')->addChild('LinearRing')->addChild('coordinates', $pp->outerBoundaryIs->LinearRing->coordinates);
    }
  }
  foreach ($template_xml->Document->Placemark as $x) {
    if (! $x->styleUrl)
      $x->addChild('styleUrl', '#spolygon-map');
  }
  $template_xml->asXML("output/{$p->name}.kml");
}

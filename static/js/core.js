var map;
var timeout_request_trigger;
var center;
var radius;
var at = 0;
var types = 0;
var ctaLayers;
var cities;
var parser;
var tll;
var ts = [];
var API_ENDPOINT = "__API_ENDPOINT__";

var init_googlemaps = function() {
    var pos = new google.maps.LatLng(25.0505073, 121.5282984);
    var mapOptions = {
        zoom: 12,
        center: pos,
        mapTypeControl: false
    }

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    parser = new geoXML3.parser({map: map, zoom: false, afterParse: after_parse});

    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            function(position) {
                pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                map.setCenter(pos);
                map.setZoom(12);
            }
        )
    }

    google.maps.event.addListener(map, 'bounds_changed', function() {
        clearTimeout(timeout_request_trigger);
        timeout_request_trigger = setTimeout(function(){
            var r2 = map.getCenter();
    	    var r1 = new google.maps.LatLng(map.getBounds().getNorthEast().lat(), r2.lng());
	        radius = Math.round(google.maps.geometry.spherical.computeDistanceBetween(r1, r2));
            center = r2.lat() + "," + r2.lng();
        	console.log("center:" + center + "; radius:" + radius);
        }, 200);
    });
    google.maps.event.addListener(map, 'mouseover', function() {
        clearTimeout(timeout_request_trigger);
        timeout_request_trigger = setTimeout(function(){
            if ($('.year_stat_and_caption').hasClass('show'))
                $('.year_stat_and_caption').removeClass('show halfshow').addClass('halfshow');
        }, 200);
    });
    google.maps.event.addListener(map, 'mouseout', function() {
        clearTimeout(timeout_request_trigger);
        timeout_request_trigger = setTimeout(function(){
            if ($('.year_stat_and_caption').hasClass('halfshow'))
                $('.year_stat_and_caption').removeClass('show halfshow').addClass('show');
        }, 200);
    });
}

var after_parse = function(docs) {
    for (q in ts) {
        clearTimeout(ts[q]);
    }
    clearTimeout(tll);
    setTimeout(layer_loop, 1000);
}

var layer_loop = function() {
    for (q in ctaLayers) {
        ts[q] = setTimeout(timer_show, q * 2000, q);
    }
    tll = setTimeout(layer_loop, (ctaLayers.length + 1) * 2000);
}

var change_color_kml_doc = function(doc, level) {
    var color;
    if      (level == 1) color = '#66ee66';
    else if (level == 2) color = '#aaffaa';
    else if (level == 3) color = '#ffffff';
    else if (level == 4) color = '#ffaaaa';
    else if (level == 5) color = '#ee6666';
    else                 color = '#ffffff';

    for (var i in doc.placemarks) {
        doc.placemarks[i].polygon.setOptions({fillColor: color});
    }
}

var find_doc_by_district_name = function(name) {
    for (var i in parser.docs) {
        if (parser.docs[i].baseUrl.split('/')[1] == name)
            return parser.docs[i];
    }
}

var timer_show = function(i) {
    $('#year_caption').text(ctaLayers[i]['year']);
    for(p in ctaLayers[i]['data']) {
        doc = find_doc_by_district_name(ctaLayers[i]['data'][p]['district_name']);
        if (doc)
            change_color_kml_doc(doc, ctaLayers[i]['data'][p]['level']);
    }
}

var query_map = function(cause) {
    $.ajax({url: API_ENDPOINT + '/map_get_rank', type: 'get',
            data: 'c=' + center + '&r=' + radius + '&cause=' + cause, dataType: 'json',
        statusCode: {
            200: function(json) {
                for (i in parser.docs) {
                    parser.hideDocument(parser.docs[i]);
                    parser.docs[i] = null;
                }
                parser.docs.length = 0;

                v_arr = [];
                ctaLayers = json;
                kml_ratio = 10;
                if (map.getZoom() > 9)  kml_ratio = 8;
                if (map.getZoom() > 10) kml_ratio = 6;
                if (map.getZoom() > 11) kml_ratio = 4;
                if (map.getZoom() > 12) kml_ratio = 2;
                if (map.getZoom() > 13) kml_ratio = 1;
                $.each(ctaLayers[0]['data'], function(k, v) {
                    v_arr.push('area/' + v['district_name'] + '/' + kml_ratio + '/');
                });
                parser.parse(v_arr);
            }
        }
    });
}

var init_city_list = function() {
    $.getJSON("static/city_dist.json", function(json) {
        cities = json;
    }).done(function() {
        $.each(cities, function(k, v) {
            $('.city').append('<option>'+v['name']+'</option>');
        });

        $('.city').on('change', function() {
            $.each(cities, function(k, v) {
                if ($('.city').val() == v['name']) {
                    $('.dist').empty();
                    $.each(v['dist'], function(x, y) {
                        $('.dist').append('<option>' + y['name'] + '</option>');
                    });
                    return false;
                }
            });
        });
    });
}

var init_age_list = function() {
    $('.age').append('<option></option>');
    for (var i = 0 ; i < 99 ; i+=5)
        $('.age').append('<option>' + i + ' ~ ' + (i+4) + ' 歲</option>');
    $('.age').append('<option>100 歲以上</option>');
}


var init_analyze_button = function() {
    $('.analyze').on('click', function() {
        $('.info').addClass('show');
        var name = $('.city').val() + $('.dist').val();
        per_death_cause_statistics(name);

        setTimeout(function() {
            $.ajax({url: 'center/' + name, type: 'get', dataType: 'json', statusCode: {
                200: function(json) {
                    map.panTo(json['results'][0]['geometry']['location']);
                }
            }});
        }, 1000);
    });
}

$(function() {
    init_googlemaps();
    init_city_list();
    init_age_list();
    init_chart();
    init_analyze_button();
    $('.setup').css({'opacity': 1, 'margin-top': '4em'});
});

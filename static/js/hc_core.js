var chart_resize;
var API_ENDPOINT = "__API_ENDPOINT__";

var init_chart = function () {
    $('#statistics').highcharts({
        chart: {
            height: 1200,
            type: 'bar'
        },
        title: {
            text: '此區域死因發生率統計'
        },
        subtitle: {
            text: '點選死因項目顯示詳細資料'
        },
        xAxis: {
            categories: [],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '發生率 (每十萬人)',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            shared: true
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                },
                point: {
                    events: {
                        click: function() {
                            var district = $('#statistics').attr('data-district');
                            per_year_statistics(district, this.category);
                            query_map(this.category);
                        }
                    }
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: 0,
            y: 20,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: '全國發生率',
            color: '#d0b7f2'
        }, {
            name: '此區發生率',
            color: '#9579ba'
        }]
    });

    $('#year_statistics').highcharts({
        title: {
            text: null
        },
        xAxis: {
            categories: [],
            title: {
                text: null
            }
        },
        yAxis: [
            {
                min: 0,
                title: {
                    text: '發生率 (每十萬人)',
                    align: 'middle'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            {
                min: 0,
                title: {
                    text: '死亡人數(人)',
                    align: 'middle'
                },
                labels: {
                    overflow: 'justify'
                },
                opposite: true
            }
       ],
        tooltip: {
            shared: true
        },
        plotOptions: {
            column: {
                dataLabels: {
                    enabled: true
                }
            },
            spline: {
                dataLabels: {
                    enabled: false
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 60,
            y: 60,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [
            {
                type: 'column',
                name: '此區死亡人數',
                yAxis: 1,
                color: '#e5e5e5'
            },{
                type: 'spline',
                name: '此區發生率',
                color: '#9579ba'
            },{
                type: 'spline',
                name: '全國發生率',
                color: '#d0b7f2'
            }
        ]
    });
};

var per_death_cause_statistics = function(district) {
    $('.result').css({'overflow-y': 'hidden'});
    $('#statistics').removeClass('show');
    $.ajax({url: API_ENDPOINT + '/chart_overview', data: 'district=' + district, dataType: 'json', type: 'get', cache: false,
        statusCode: {
            200: function(json) {
                var dist_arr = [];
                var country_arr = [];
                var category_arr = [];
                $.each(json, function(k, v) {
                    dist_arr[k] = parseFloat(v['district_ratio']);
                    country_arr[k] = parseFloat(v['country_ratio']);
                    category_arr[k] = v['cause'];
                })
                $('#statistics').highcharts().setTitle({text: district + ' 各死因發生率統計 (每十萬人)'});
                $('#statistics').attr('data-district', district);
                $('#statistics').highcharts().xAxis[0].categories = category_arr;
                $('#statistics').highcharts().series[0].setData(country_arr);
                $('#statistics').highcharts().series[1].setData(dist_arr);
                setTimeout(function() {
                    $('#statistics').addClass('show');
                    $('.result').css({'overflow-y': 'scroll'});
                }, 300);
            },
            404: function() {
            }
        }
   });
}

var per_year_statistics = function(district, cause) {
    $('.year_stat_and_caption').removeClass('show halfshow');
    $.ajax({url: API_ENDPOINT + '/chart_history', dataType: 'json', type: 'get',
            data: 'district=' + district + '&cause=' + cause, cache: false,
        statusCode: {
            200: function(json) {
                var year_arr = [];
                var death_arr = [];
                var dist_arr = [];
                var country_arr = [];
                $.each(json, function(k, v) {
                    year_arr[k] = v['year'];
                    death_arr[k] = parseInt(v['death_num']) || 0;
                    dist_arr[k] = parseFloat(parseFloat(v['district_ratio']).toFixed(2)) || 0;
                    country_arr[k] = parseFloat(parseFloat(v['country_ratio']).toFixed(2)) || 0;
                })
                $('#year_statistics').highcharts().setTitle({text: '近20年發生率(每十萬人) 與人數統計'}, {text: district  + ' - ' + cause});
                $('#year_statistics').highcharts().xAxis[0].categories = year_arr;
                $('#year_statistics').highcharts().series[0].setData(death_arr);
                $('#year_statistics').highcharts().series[1].setData(dist_arr);
                $('#year_statistics').highcharts().series[2].setData(country_arr);
                setTimeout(function() {
                    $('.year_stat_and_caption').addClass('show');
                }, 300);
            },
            404: function() {
            }
        }
    });
}
